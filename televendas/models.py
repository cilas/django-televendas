import datetime
from django.db import models
from django.core.mail import send_mail
from cpffield import cpffield

class ComissionPlan(models.Model):
    """
    Model for Comission Plan
    """
    lower_percentage = models.FloatField("Menor Porcentagem", help_text="The lower comission percentage")
    min_value = models.DecimalField("Valor Mínimo", max_digits=8, decimal_places=2, help_text="The minimum amount to receive bigger comission")
    upper_percentage = models.FloatField("Maior Porcentagem", help_text="The bigger comission percentage")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ["created_at"]
        verbose_name_plural = "Planos de Comissões"

    def __str__(self):
        return 'Plano {} - {}%'.format(self.min_value, self.upper_percentage)



class Seller(models.Model):
    """
    Model for Seller
    """
    name = models.CharField("Name", max_length=200, help_text="Name of Seller")
    address = models.TextField("Endereço", help_text="Address of Seller")
    phone = models.CharField("Telefone", max_length=20, help_text="Phone number of Seller")
    birthday = models.DateField("Data de Nascimento", help_text="Birthday of Seller")
    email = models.EmailField(max_length=254, help_text="Email of Seller")
    cpf = cpffield.CPFField('CPF', max_length=14, help_text="CPF of Seller")
    comission_plan = models.ForeignKey(ComissionPlan, related_name='sellers', on_delete=models.CASCADE, verbose_name="Plano de Comissãos", help_text="Comission Plan of Seller")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
   
    @property
    def age(self):
        "Retorna a idade do vendedor."
        birthday = datetime.datetime.strptime(str(self.birthday), '%Y-%m-%d')
        today = datetime.date.today()
        return today.year - birthday.year - ((today.month, today.day) < (birthday.month, birthday.day))

    @property
    def great_comission(self):
        "retorna maior comissao."
        try:
            sale = Sale.objects.filter(seller_id=self.id).order_by('-comission').first() 
            return sale.comission
        except Exception as e:
            return 0.00
        


    def __str__(self):
        return self.name

    class Meta:
        ordering = ["created_at"]


class Sale(models.Model):
    """
    Model for Sale
    """
    MESES = (
        ('1', 'Janeiro'),
        ('2', 'Fevereiro'),
        ('3', 'Março'),
        ('4', 'Abril'),
        ('5', 'Maio'),
        ('6', 'Junho'),
        ('7', 'Julho'),
        ('8', 'Agosto'),
        ('9', 'Setembro'),
        ('10', 'Outubro'),
        ('11', 'Novembro'),
        ('12', 'Dezembro'),
    )
    seller = models.ForeignKey(Seller, related_name='sales', on_delete=models.CASCADE)
    month = models.CharField("Mês", choices=MESES, max_length=2, help_text="month from sales")
    amount = models.DecimalField("Valor das Vendas", max_digits=8, decimal_places=2, help_text="The total of sales on month")
    comission = models.DecimalField("Valor da Comissão", max_digits=8, decimal_places=2, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    
    def save(self, *args, **kwargs):
        comission_plan = self.seller.comission_plan
        if self.amount < comission_plan.min_value:
            self.comission = (comission_plan.lower_percentage/100)*float(self.amount)
        self.comission = (comission_plan.upper_percentage/100)*float(self.amount)
        super(Sale, self).save(*args, **kwargs)
        
    class Meta:
        ordering = ["created_at"]
        verbose_name_plural = "Vendas"
        unique_together = ("seller", "month")  

    def __str__(self):
       return 'Vendas - {}'.format(self.month)

