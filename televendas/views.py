from rest_framework import status, viewsets
from rest_framework import generics
from televendas.serializers import ComissionPlanSerializer, SaleSerializer, SellerSerializer
from .models import ComissionPlan, Sale, Seller
from rest_framework.response import Response
from django.db.models import Max
from rest_framework.decorators import action, api_view
from django.forms.models import model_to_dict
from django.core.mail import send_mail

class ComissionPlanViewSet(viewsets.ModelViewSet):
    queryset = ComissionPlan.objects.all()
    serializer_class = ComissionPlanSerializer

class SellerViewSet(viewsets.ModelViewSet):
    """
    retrieve:
    Return the given Seller.

    list:
    Return a list of all the existing Sellers.

    create:
    Create a new Seller instance.
    """
    queryset = Seller.objects.all()
    serializer_class = SellerSerializer

    @action(detail=False, methods=['get'], url_path='comissions/(?P<month>[^/.]+)',)
    def comissions(self, request, month):
        """
        get:
        Return a list of Sellers ordereds by value of comission on month.
        """
        sellers = Seller.objects.filter(sales__month=month).annotate(comission=Max('sales__comission')).values('id', 'name', 'cpf', 'email', 'phone', 'comission').order_by("-comission")
        page = self.paginate_queryset(sellers)
        if page is not None:
            return Response(page)
        return Response(sellers)

class SaleViewSet(viewsets.ModelViewSet):    
    queryset = Sale.objects.all()
    serializer_class = SaleSerializer


@api_view(['POST'])
def check_comission(request):
    """
    post:
    Check if amount sale is great than avarage of the last five months.
    """
    if isinstance(request.data['seller'], int):
        seller = Seller.objects.get(pk=request.data['seller'])
        sales = Sale.objects.filter(seller_id=seller.id).values_list('amount', flat=True).order_by('-month')[:5]
        if len(sales) > 0:            
            sum = 0.0
            notify = False
            for idx, val in enumerate(sorted(sales), 1):
                sum +=  float(val) * float(idx)
            avg = sum/len(sales)
            minimum_amount = avg - (avg * 10 / 100)
            try:
                if isinstance(request.data['amount'], float):
                    if minimum_amount > request.data['amount']:
                        notify = True
                        try:
                            send_mail(
                                'Notification about sales',
                                'Your sales are below average',
                                'notify@televendas.local',
                                [seller.email],
                                fail_silently=False,
                            )
                        except Exception as e:
                            pass
                    else:
                        notify = False
                else:
                    return Response({'error': 'amount must be float type'}, status=status.HTTP_400_BAD_REQUEST)
            except Exception as e:
                pass
            return Response({'notify': notify})
