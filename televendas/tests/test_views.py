from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from ..models import ComissionPlan, Seller, Sale
from .factories import ComissionPlanFactory, SellerFactory

class ComissionPlanViewTestCase(APITestCase):
    def test_create_ComissionPlan(self):
        """
        Ensure we can create a new ComissionPlan object.
        """
        url = reverse('comissionplan-list')
        comission_plan_data = {
                "lower_percentage": 2,
                "min_value": 3000.0,
                "upper_percentage": 5,
        }
        response = self.client.post(url, comission_plan_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(ComissionPlan.objects.count(), 1)
        self.assertEqual(ComissionPlan.objects.get().min_value, 3000.0)

class SellerViewTestCase(APITestCase):
    def test_create_seller(self):
        """
        Ensure we can create a new seller object.
        """
        url = reverse('seller-list')
        #create a comission plan id 1
        comission_plan = ComissionPlanFactory()
        seller_data = {
            'name': 'John Doe', 
            'address': 'Rua dos Maristas, 7, 7', 
            'phone': 7192680548, 
            'birthday': '1992-10-24', 
            'email': 'johndoe@gmail.com',
            'cpf': 92294814088,
            'comission_plan': 1
        }
        response = self.client.post(url, seller_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Seller.objects.count(), 1)
        self.assertEqual(Seller.objects.get().name, 'John Doe')

class SaleViewTestCase(APITestCase):
    def test_create_sale(self):
        """
        Ensure we can create a new Sale object.
        """
        url = reverse('sale-list')
        #create a comission plan id 1
        self.seller = SellerFactory()
        sale_data = {
            'seller': 1,
            'month': 1,
            'amount': 1000.0
        }
        response = self.client.post(url, sale_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Sale.objects.count(), 1)
        self.assertEqual(Sale.objects.get().amount, 1000.0)