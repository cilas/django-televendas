from django.urls import include, path
from rest_framework import routers
from .views import ComissionPlanViewSet, SellerViewSet, SaleViewSet, check_comission
from rest_framework.documentation import include_docs_urls

router = routers.DefaultRouter()
router.register(r'comissions', ComissionPlanViewSet)
router.register(r'sellers', SellerViewSet)
router.register(r'sales', SaleViewSet)

urlpatterns = [
    path('api/v1/', include(router.urls)),
    path('api/v1/check-comission/', check_comission),
    path('', include_docs_urls(title='Televendas API'))
]

